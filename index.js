/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userInfo(){
	let fullName = prompt("Enter your full name.");
	let userAge = prompt("Enter your age.");
	let userAddress = prompt("Enter your address.");
	console.log("Hello, " + fullName);
	console.log("You are " + userAge + " years old.");
	console.log("You live in " + userAddress + ".");
	alert ("Thank you for your input!");
}	
userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function musicalArtist(){
	let astists = ["Chris Brown", "Usher", "Snoop Dogg", "Black Pink", "SB19" ];
	console.log("1. " + astists[0]);
	console.log("2. " + astists[1]);
	console.log("3. " + astists[2]);
	console.log("4. " + astists[3]);
	console.log("5. " + astists[4]);
}	
musicalArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function topMovies(){
	let movieTitle = [
						"Marvel's the Avengers",
						 "Avengers: Age of Ultron", 
						 "Avengers: Infinity War ",
						 "Avengers: Endgame ", 
						 "Captain America: Civil War" 
						];
	let movieRating = ["91%", "76%", "85%", "94%", "90%"];
	console.log("1. " + movieTitle[0]);
	console.log("Rotten Tomatoes Rating: " + movieRating[0]);
	console.log("2. " + movieTitle[1]);
	console.log("Rotten Tomatoes Rating: " + movieRating[1]);
	console.log("3. " + movieTitle[2]);
	console.log("Rotten Tomatoes Rating: " + movieRating[2]);
	console.log("4. " + movieTitle[3]);
	console.log("Rotten Tomatoes Rating: " + movieRating[3]);
	console.log("5. " + movieTitle[4]);
	console.log("Rotten Tomatoes Rating: " + movieRating[4]);
}	
topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


printFriends();
	function printFriends(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



